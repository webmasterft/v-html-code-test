'use strict'
const gulp = require('gulp')
const gutil = require('gulp-util');
const plumber = require('gulp-plumber')
const sass = require('gulp-sass')
const sourcemaps = require('gulp-sourcemaps')
const webserver = require('gulp-webserver')
const concat = require('gulp-concat')
const uglify = require('gulp-uglify');
const autoprefixer = require('gulp-autoprefixer');
const strip = require('gulp-strip-comments');


gulp.task('sass', () => {
  return gulp
    .src([
      './src/sass/**/*.scss'
    ])
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
    .pipe(concat({ path: 'main.css' }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('./dist/css'))
})


gulp.task('copy-assets', function() {
    gulp.src(['./src/assets/*.*', '!./src/assets/*.zip', '!./src/assets/*.svg']).pipe(gulp.dest('./dist/assets'))
})

gulp.task('copy-html', function() {
    gulp.src(['./src/index.html']).pipe(gulp.dest('./dist/'))
})

gulp.task('setWatch', function() {
  global.isWatching = true;
});



gulp.task('webserver', () => {
  gulp.src('./dist').pipe(
    webserver({
      livereload: true,
      directoryListing: false,
      open: true
    })
  )
})

gulp.task('watch', () => {
  gulp.watch('./src/sass/**/*.scss', ['sass'])
})

gulp.task('default', [
  'sass',
  'copy-assets',
  'copy-html',
  'setWatch',
  'watch',
  'webserver'
])
